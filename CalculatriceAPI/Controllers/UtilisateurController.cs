﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CalculatriceAPI.Controllers
{
    //faire un CRUD

    public class UtilisateurDTO
    { 
    using DTO;

    [Route("api/utilisateurs")]
    [ApiController]
    public class UtilisationController : ControllerBase
    {
        private static List<UtilisateurDTO> utilisateurs = new List<UtilisateurDTO>();

        [HttpGet]
        public IEnumerable<UtilisateurDTO> FindAll()
        {
            return utilisateurs;
        }

        [HttpPost]
        public void Save([FromBody] UtilisateurDTO value)
        {
            utilisateurs.Add(value);
        }

        [HttpGet]
        [Route("{id}")]
        public UtilisateurDTO FindById(int id)
        {
            return utilisateurs[id];
        }
        [HttpGet]
        [Route("nom/{nom}")]
        public UtilisateurDTO findByNom(int nom)
        {
            return utilisateurs[nom];
        }

        [HttpGet]
        [Route("prenom/{prenom}")]
        public UtilisateurDTO findByPrenom(int prenom)
        {
            return utilisateurs[prenom];
        }

        [HttpGet]
        [Route("age/{age}")]
        public UtilisateurDTO findByAge(int age)
        {
            return utilisateurs[age];
        }

        [HttpGet]
        [Route("metier/{metier}")]
        public UtilisateurDTO findByMetier(int metier)
        {
            return utilisateurs[metier];
        }

        [HttpPut("{id}")]
        public UtilisateurDTO Update(int id, [FromBody] UtilisateurDTO value)
        {
            value.Id = id;
            utilisateurs[id] = value;
            return value;
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            utilisateurs[id] = null;
        }
    }
}

